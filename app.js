var app = new Vue({
	el: '#app',
	data: {
		playerHealth: 100,
		monsterHealth: 100,
		isGameRunning: false,
		logs: [],
		specialAttackUsed: false,
	},
	methods: {
		startGame() {
			this.isGameRunning = true;
			this.playerHealth = 100;
			this.monsterHealth = 100;
			this.logs = [];
			this.specialAttackUsed = false;
		},
		attack() {
			var damage = this.getDamage(3, 10);
			this.monsterHealth = Math.max(0, this.monsterHealth - damage);
			this.logs.unshift({
				isPlayer: true,
				text: 'Player hits Monster with ' + damage + ' damage',
			});
			
			if (this.checkWin())
				return;

			this.monsterAttack();
		},
		specialAttack() {
			this.specialAttackUsed = true;
			var damage = this.getDamage(10, 20);
			this.monsterHealth = Math.max(0, this.monsterHealth - damage);
			this.logs.unshift({
				isPlayer: true,
				text: 'Player hits Monster with Special Attack with ' + damage + ' damage',
			});

			if (this.checkWin())
				return;

			this.monsterAttack();
		},
		heal() {
			this.playerHealth = Math.min(this.playerHealth + 10, 100);
			this.logs.unshift({
                isPlayer: true,
                text: 'Player heals for 10',
			});
			
			this.monsterAttack();
		},
		monsterAttack() {
			var damage = this.getDamage(5, 12);
			this.playerHealth = Math.max(0, this.playerHealth - damage);

			this.checkWin();

			this.logs.unshift({
                isPlayer: false,
                text: 'Monster hits Player for ' + damage + ' damage',
            });
		},
		giveUp() {
			if (!confirm('Are you sure you want to give up?'))
				return;

			this.isGameRunning = false;
		},
		checkWin() {
			if (this.monsterHealth <= 0) {
                if (confirm('You won! New Game?'))
                    this.startGame();
                else
                    this.gameIsRunning = false;
                
                return true;
			}
			else if (this.playerHealth <= 0) {
                if (confirm('You lost! New Game?'))
                    this.startGame();
                else
                    this.gameIsRunning = false;
                
                return true;
			}
			
            return false;
		},
		getDamage(min, max) {
			return Math.max(min, Math.floor(Math.random() * max));
		}
	},
});